package com.tema4.samplegetimages;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_GALLERY = 10;
    private static final int REQUEST_CAMERA = 11;
    private static final int REQUEST_PERMISSION_CAMERA = 5;
    private static final String FOLDER_NAME = "SampleGetImages";
    private static final String APP_TAG = "Get File App";

    private ImageView imageView;
    private String mImageString;
    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);

        // si se ha almacenado algo en el Bundle de estado, lo recuperamos
        if (savedInstanceState != null && savedInstanceState.containsKey("foto")) {
            mImageString = savedInstanceState.getString("foto");
            mImageUri = Uri.parse(mImageString);
            imageView.setImageURI(mImageUri);
        }

        // inicia la seleccion de imágenes de la galería
        Button gallery = findViewById(R.id.btn_gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromGallery();
            }
        });
        // inicia la seleccion de imágenes de la cámara
        Button button = findViewById(R.id.btn_camera);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissions();
            }
        });
    }

    /**
     * Método para comprobar los permisos para escribir en la memoria externa del teléfono (WRITE_EXTERNAL_STORAGE)
     */
    private void checkPermissions() {
        // si hay permisos, se inicia la cámara, si no se han concedido permisos, se piden al usuario
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            fromCamera();
        } else {
            String[] array = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(this, array, REQUEST_PERMISSION_CAMERA);
        }
    }

    /**
     * Inicia el intent para seleccionar una imagen desde la galería
     */
    private void fromGallery() {
        Intent contentSelector = new Intent(Intent.ACTION_OPEN_DOCUMENT); // permite al usuario seleccionar cualquier tipo de dato del dispositivo
        contentSelector.addCategory(Intent.CATEGORY_OPENABLE); // se indica que los resultados deben venir en formato Uri
        contentSelector.setType("image/*"); // se indica el tipo de datos y el formato (imagenes en cualquier formato)
        startActivityForResult(contentSelector, REQUEST_GALLERY); //inicia el intent esperando un resultado, que vendrá en onActivityResult
    }

    // Returns the File for a photo stored on disk given the fileName

    /**
     * Crea un archivo con un nombre dado en una carpeta dentro de Pictures
     *
     * @param fileName - nombre del archivo
     * @return - objeto de tipo file que hace referencia al archivo creado
     */
    public File getPhotoFileUri(String fileName) {
        // path será la ruta donde se va a crear el archivo. Con el método getExternalStorage... se accede al almacenamiento público
        // la ruta final será: almacenamiento internt/Pictures/SampleGetImages
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + FOLDER_NAME;
        File mediaStorageDir = new File(path); //crea un archivo en el directorio de la ruta anterior

        // crea el directorio si no existe
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        // Devuelve el archivo donde se almacenará la imagen
        File file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    /**
     * Métod para crear un nombre de archivo basado en la fecha
     *
     * @return String para nombre de archivo
     */
    private String createFileName() {
        String formato = "yyyyMMdd_HHmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.getDefault());

        Date ahora = new Date();
        String timeStamp = sdf.format(ahora);
        return "IMG_" + timeStamp + ".png";
    }

    /**
     * Inicia el intent para cogeru una imagen desde la cámara
     */
    private void fromCamera() {

        // intent para tomar fotografías y devolver la imagen a la app
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // crea el archivo donde se almacenará la imagen de la cámara
        String nombreArchivo = createFileName();
        File photoFile = getPhotoFileUri(nombreArchivo);

        // Si la versión de la App es superior a la 29 (Android X)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // clase que permite agrupar un conjunto de valores para que puedan ser procesados
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DISPLAY_NAME, nombreArchivo); //nombre del archivo
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/png"); // formato
            values.put(MediaStore.MediaColumns.RELATIVE_PATH, "Pictures/" + FOLDER_NAME); // ruta donde se almacenará la imagen

            // se inserta la información en el almacenamiento externo. getContentResolver devuelve la uri con la ruta tipo "content" a la imagen
            mImageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            // para el almacenamiento interno, sería  mImageUri = getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
        } else {
            // Si la versión de la App es superior a la 24 (Nougat)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // insertar el archivo en el provider (proveedor de contenido)
                mImageUri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
            } else {
                mImageUri = Uri.fromFile(photoFile);
            }
        }
        // se pasa el uri con la info donde se almacenará la imagen, al intent,
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);

        // si hay apps en el dispositivo que puedan hacer fotos
//        if (intent.resolveActivity(getPackageManager()) != null) {
        // inicia el intent para tomar la foto
        startActivityForResult(intent, REQUEST_CAMERA);
//        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GALLERY) {
            if (resultCode == RESULT_OK) {
                // si los datos no son nulos
                if (data != null && data.getDataString() != null) {
                    // se obtiene la ruta de la imagen como String y despues la uri
                    mImageString = data.getDataString();
                    Uri uri2 = Uri.parse(mImageString);
                    // o se puede obtener directamente la uri
                    Uri uri = data.getData();

                    // se pone la imagen en el imageview a traves de la uri
                    imageView.setImageURI(uri2);

                    // alternativa usando la librería picasso
                    // Picasso.get().load(dataString).into(imageView);
                }
            }
        } else if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
//                Picasso.get().load(miImagen).into(imageView);

                mImageString = mImageUri.toString();
                // ya teniamos la uri donde se iba a almacenar la imagen de la cámara
                imageView.setImageURI(mImageUri);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAMERA) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fromCamera();
            } else {
                Toast.makeText(this, "No puede utilizar esta funcionalidad sin aceptar los permisos", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * Si cambia el estado del dispositivo
     *
     * @param outState -> bundle para almacenar datos que se puedan perder
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // si ya se ha almacenado la ruta como String en la variable, la almacenamos en el bundle
        if (mImageString != null && !mImageString.isEmpty()) {
            outState.putString("foto", mImageString);
        }
    }
}